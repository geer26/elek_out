import time
import os
import sqlite3


def db_test():
    con = sqlite3.connect(os.environ.get("DB_NAME") or "elek_out.db")

def get_db():
    return sqlite3.connect(os.environ.get("DB_NAME") or "elek_out.db")


def test():
    t = int(os.environ.get('TIME_CHECK') or 5)
    while True:
        os.system(f"echo 'TEST ...'")
        time.sleep(t)

if __name__ == "__main__":
    test()